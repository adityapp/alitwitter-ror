# Ali Twitter

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet
3. List tweets


## Ruby version

The required Ruby version is **2.6.5**


## Database migration

```
rails db:migrate
```


## How to run the test suite

```
rails test
```

## How to run

```
rails server
```

using browser and put `http://localhost:3000/tweets`


## How to build Docker Image Artefact

You should have installed [Docker](https://docs.docker.com/) in your machine

To build Docker Image
```
docker build -t <IMAGE_NAME> .
```

To run Docker Image in container
```
docker container run -p 3000:3000 <IMAGE_NAME>
```

Then you can access from browser with `localhost:3000`


## How to run Vitual Machine
You should have installed [Vagrant](https://www.vagrantup.com/docs/installation/) and [Ansible](https://docs.ansible.com/intro_installation.html#installing-the-control-machine) in your machine

To run `Vagrant` with:
```
vagrant up
```

To reload provisioning with:
```
vagrant reload --provision
```


### Provisioning Commands

Provisioning List:
 - `deploy` : Install Docker Engine, Load Docker Image, and Run container (by default `deploy` will be execute)
 - `redeploy` : Remove existing container, load Docker Image, and Run container
 - `remove` : Stop and remove existing container
 - `start` : Start a stopped container
 - `stop` : Stop a running container

```
vagrant provision --provision-with <PROVISIONING_NAME>
```

For example:

```
vagrant provision --provision-with redeploy
```
